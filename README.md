# Machine Learning in Travel Management Essay

Travel management may be understood as organising and understanding the travel route of travelling entities or the transport
network system overall to conserve resources.


This essay focuses on three aspects of travel management:
* Finding the route with the shortest travel time.
* Urban traffic lights control strategies optimisation for minimising delays.
* Travel demand predictions for travel industry businesses.


This essay contains existing research on the topic of machine learning implementation in the travel industry.
It also summarizes some of the main algorithms used in the field.


* This essay was submitted for an assessment in my module "Machine Learning". 
* Please read the essay here: [Machine Learning in Travel Management Essay](Machine Learning in Travel Management.pdf)
